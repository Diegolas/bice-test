# Proyecto postulación React App

Proyecto creado con [Create React App](https://github.com/facebook/create-react-app).\
Autor: Diego Eduardo San Martín

## Acerca del proyecto

El proyecto contempla varias llamadas a la api de https://www.indecon.online/date/:key/:date la cual entrega un valor particular de un indicador según la fecha dada. Con esto se armó tarjetas que indican y comparan varios indicadores entre una fecha principal y otra a comparar. Cuando el indicador principal posee un valor al indicador a comparar se muestra en rojo, por el contrario azul. Cuando no hay datos de la fecha proporcionada no hace la comparación.

### `yarn || npm install`

Instala las dependencias del proyecto.\

### `yarn start || npm start`

Inicia el proyecto en modo desarrollo, por defecto utiliza .\
[http://localhost:3000](http://localhost:3000) .

El proyecto cuenta con un proxy a [https://www.indecon.online/](https://www.indecon.online/) por lo que todas las llamadas a la API están redireccionadas a ese sitio .\


### `yarn test || npm test`

Inicia los test para el proyecto. Actualmente posee uno que es el componente principal Main.test.js. Realiza la simulación, hace un mock de la llamada a la api y cambia el valor de este para una segunda llamada.
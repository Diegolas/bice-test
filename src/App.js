import './App.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Main from "./pages/main/Main";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/">
            <Main/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;

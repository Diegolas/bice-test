import './Indicator.scss';
import PropTypes from 'prop-types';
import { indicatorsToType } from "../../utils/conts";

const getColor = (first, second) => {
    if(!first.value || !second.value || first.value === second.value ) {
        return 'default';
    }
    if(first.value > second.value) {
        return 'blue';
    }
    else {
        return 'red';
    }
}

const Indicator = ({indicator}) => {
    const [first, second] = indicator;
    
    return (
        <div className="indicator">
            <div className="indicator__name">{`${first.key}`}</div>
            <div className="indicator__sub-name">{first.name}</div>
            <div className={`indicator__value ${getColor(first, second)}`}>
                {first.value && <div className="indicator__value-type">{indicatorsToType[`${first.key}`].simbol}</div>}
                <div className="indicator__value-value">{
                        first.value ? indicatorsToType[`${first.key}`].parser(first.value) : 'No hay valor'
                    }
                </div>
            </div>
            <div className="indicator__value-compare">
                <div>Comparado con:</div>
                <div>
                    {second.value && <div className="indicator__value-type">{indicatorsToType[`${second.key}`].simbol}</div>}
                    <div className="indicator__value-value"> {
                            second.value ? indicatorsToType[`${second.key}`].parser(second.value) : 'No hay valor'
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

Indicator.propTypes = {
    indicator: PropTypes.array,
}

export default Indicator;

import './Title.scss';
import PropTypes from 'prop-types';

const Title = ({title, subtitle}) => {
  return (
    <div className="title">
        <div className="title__main">{title}</div>
        <div className="title__separator" >{`>>`}</div>
        <div className="title__subtitle">{subtitle}</div>
    </div>
  );
}

Title.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
}

export default Title;

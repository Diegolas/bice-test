import Title from "../../components/title/Title";
import { useEffect, useState } from "react";
import { getAllIndicators } from "../../services/indicators.service";
import Indicator from "../../components/indicator/Indicator";
import Loader from "../../components/loader/Loader";
import DatePicker  from "react-datepicker";
import moment from "moment";
import './Main.scss';
import "react-datepicker/dist/react-datepicker.css";

const Main = () => {
    const [indicators, setIndicators] = useState([]);
    const [loading, setLoading] = useState(true);
    const [startDate, setStartDate] = useState(new Date(2019, 1, 22));
    const [compareDate, setCompareDate] = useState(new Date(2019, 1, 25));

    const getAll = async(date, secondDate) => {
        setLoading(true);
        const resp = await getAllIndicators(date, secondDate);
        setIndicators(resp);
        setLoading(false);
    }

    const changeStartDate = (date) => {
        getAll(moment(date), moment(compareDate));
        setStartDate(date);
    }
    const changeCompareDate = (date) => {
        getAll(moment(startDate), date);
        setCompareDate(date);
    }

    useEffect( () => {
        getAll(moment(startDate), moment(compareDate));
    }, [startDate, compareDate])

    return (
    <div className="main">
        <Title title="Indicadores económicos" subtitle="comparación de valores entre fechas" />
        <div>
            {loading && <Loader />}
            <div className="main__datepicker">
                <div>
                    <h4>Fecha principal</h4>
                    <DatePicker id="startDate" selected={startDate} onChange={changeStartDate} dateFormat="dd/MM/yyyy" />
                </div>
                <div>
                    <h4>Fecha a comparar</h4>
                    <DatePicker selected={compareDate} onChange={changeCompareDate} dateFormat="dd/MM/yyyy" />
                </div>
            </div>
            <div className="main__indicators">
                {indicators.map((value, key) => 
                    <Indicator indicator={value} key={key} />
                )}
            </div>
        </div>
        
    </div>
    );
}

export default Main;

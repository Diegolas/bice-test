import { render, waitForElementToBeRemoved, fireEvent } from '@testing-library/react';
import Main from './Main';
import axiosMock from "axios";

const resp = () => Promise.resolve({data: {"key":"ivp","name":"Indice de valor promedio","unit":"pesos","date":1549411200,"value":28751.79}});
const resp2 = () => Promise.resolve({data: {"key":"yen","name":"Yen","unit":"dolar","date":1550793600,"value":110.8}});

test('renders Main', async () => {
  axiosMock.get.mockImplementation(resp);
  const component = render(<Main />);
  await waitForElementToBeRemoved( () => component.container.querySelector('.loader') );
  expect(component.container.querySelectorAll('.indicator')).toHaveLength(10);
  expect(component.container.querySelector('.indicator .indicator__name').innerHTML).toEqual('ivp');
  expect(component.container.querySelector('.indicator .indicator__sub-name').innerHTML).toEqual('Indice de valor promedio');
  expect(component.container.querySelector('.indicator .indicator__value-value').innerHTML).toEqual('28.751,79');
  axiosMock.get.mockImplementation(resp2);
  fireEvent.change(component.container.querySelector('#startDate'), {
    target: { value: '23/02/2019' },
  });
  await waitForElementToBeRemoved( () => component.container.querySelector('.loader') );
  expect(component.container.querySelector('.indicator .indicator__name').innerHTML).toEqual('yen');
});

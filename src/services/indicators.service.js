import axios from "axios";
import moment from "moment";
import { indicatorsArry } from "../utils/conts";

const format = 'DD-MM-YYYY';

const getSimpleIndicator = ( indicator, date ) => {
    return axios.get(`/date/${indicator}/${date}`).then( resp => resp.data );
    //return mock;
}

const getTodayAndYesterdayIndicator = async (indicator, date, secondDate) => {
    const today = getSimpleIndicator(indicator, date.format(format));
    const yesterday = getSimpleIndicator(indicator, secondDate.format(format));
    return await Promise.all([today, yesterday]);
}

const getAllIndicators = async(date, secondDate) => {
    const allIndicators = indicatorsArry.map(value => getTodayAndYesterdayIndicator(value, moment(date), moment(secondDate)));
    return await Promise.all(allIndicators);
}

export {
    getSimpleIndicator,
    getAllIndicators
}
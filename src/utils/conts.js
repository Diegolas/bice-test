export const indicatorsArry = ['cobre','dolar','euro','ipc','ivp','oro','plata','uf','utm','yen'];

const usdFormatter = (value) => new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
    }).format(value);

const eurFormatter = (value) => new Intl.NumberFormat('it-IT', {
    style: 'currency',
    currency: 'EUR'
  }).format(value);

const japFormatter = (value) => new Intl.NumberFormat('ja-JP', {
    style: 'currency',
    currency: 'JPY'
  }).format(value);
  
const clpFormatter = (value) => new Intl.NumberFormat('de-DE', {}).format(value);


export const indicatorsToType = {
    'cobre': {simbol: 'USD', parser: usdFormatter},
    'dolar': {simbol: 'USD', parser: usdFormatter},
    'euro': {simbol: '', parser: eurFormatter},
    'ipc': {simbol: '%', parser: clpFormatter},
    'ivp': {simbol: 'CLP$', parser: clpFormatter},
    'oro': {simbol: 'USD', parser: usdFormatter},
    'plata': {simbol: 'USD', parser: usdFormatter},
    'uf': {simbol: 'CLP$', parser: clpFormatter},
    'utm': {simbol: 'CLP$', parser: clpFormatter},
    'yen': {simbol: '', parser: japFormatter},
};